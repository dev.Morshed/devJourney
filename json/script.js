// console.log("ok");
const url = ('https://jsonplaceholder.typicode.com/posts');

fetch(url)
.then(res => res.json())
.then(data => loadUser(data));


function loadUser(data){
  const blogBox = document.getElementById('blogs');
  for(const b of data){
    const li = document.createElement('li');
    li.innerHTML = `
    <div>
      <h5>${b.id}</h5>
      <h2>${b.title}</h2>
      <p>${b.body}</p>
    </div>
    `;
    blogBox.appendChild(li);
  }
}

