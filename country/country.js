// console.log("Countried"); 

const apiUrl = 'https://restcountries.com/v3.1/all';

const loadData = () =>{
  fetch(apiUrl)
  .then(res => res.json())
  .then(data => displayData(data))
}

const displayData = (countries) =>{
  const container = document.getElementById('container')
  countries.forEach(country => {
    const div = document.createElement('div');
    div.innerHTML = `
      <h4>Country Name: <span class="text-blue"> ${country.name.common} </span> </h4>
      <h4>Capital Name: <span class="text-blue"> ${country.capital} </span> </h4>
      <img src=${country.flags.png}></img>
      <button onclick=${detailData()} type = "submit">Details</button>
    `;
    container.appendChild(div)
  });
}


const detailData = () =>{
  // console.log("btn function working");
  
}