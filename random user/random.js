

const url = 'https://randomuser.me/api/?results=10'

const fetchUser = () =>{
  fetch(url)
  .then(res => res.json())
  .then(data => displayUser(data.results))
}




/*
const loadUser = (data) =>{
  const ul = document.getElementById('user-list');
  for(const user of data){
    const li = document.createElement('li')
    li.innerHTML = `
      <div>
        <p>First Name: ${data.results}</p>
      </div>
    `;

    ul.appendChild(li)
    console.log(data.results);
  }
}

*/

const displayUser = data =>{
  // console.log(user);
  const ul = document.getElementById('user-list');
  for(const user of data){
    const li = document.createElement('li')
    li.innerHTML = `
      <div>
        <p>Name: ${user.name.title} ${user.name.first} ${user.name.last}</p>
        <p>Email: ${user.email}</p>
        <p>Phone: ${user.phone}</p>
        <p>Location: ${user.location.city}, ${user.location.state}, ${user.location.country}, (${user.location.postcode}) </p>
        <p>Age: ${user.dob.age}</p>
      </div>
    `;

    ul.appendChild(li)
    console.log(user);
  }
}